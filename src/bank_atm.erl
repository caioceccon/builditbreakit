-module(bank_atm).
-behaviour(gen_server).
-behaviour(ranch_protocol).

%% API.
-export([start_link/4]).

%% gen_server.
-export([init/1]).
-export([init/4]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-define(TIMEOUT, 10 * 1000).

-record(state, {socket, transport}).

%% API.

start_link(Ref, Socket, Transport, Opts) ->
    proc_lib:start_link(?MODULE, init, [Ref, Socket, Transport, Opts]).

%% gen_server.

%% This function is never called. We only define it so that
%% we can use the -behaviour(gen_server) attribute.
init([]) -> {ok, undefined}.

init(Ref, Socket, Transport, _Opts = []) ->
    ok = proc_lib:init_ack({ok, self()}),
    ok = ranch:accept_ack(Ref),
    ok = Transport:setopts(Socket, [{active, once}]),
    gen_server:enter_loop(?MODULE, [],
        #state{socket=Socket, transport=Transport},
        ?TIMEOUT).

handle_info({tcp, Socket, Data}, State=#state{
            socket=Socket, transport=Transport}) ->
    Transport:setopts(Socket, [{active, once}]),
    case jsx:is_json(Data) of
        true ->
            DecodeJson = jsx:decode(Data),
            TransactionResult = process_transaction(parse_params(DecodeJson)),
            Transport:send(Socket, TransactionResult);
        false ->
            Msg = io_lib:format("Invalid Json ~p", [Data]),
            io:format("protocol_error~n", []),
            Transport:send(Socket, Msg)
    end,
    {noreply, State};

handle_info({tcp_closed, Socket}, State) ->
    ok = gen_tcp:close(Socket),
    {stop, normal, State};

handle_info({tcp_error, _, Reason},
            State=#state{socket=Socket, transport=Transport}) ->
    io:format("protocol_error~n"),
    Transport:send(Socket, io_lib:format("63~n", [])),
    ok = gen_tcp:close(Socket),
    {stop, Reason, State};

handle_info(timeout, State=#state{socket=Socket, transport=Transport}) ->
    io:format("protocol_error~n"),
    Transport:send(Socket, io_lib:format("63~n", [])),
    ok = gen_tcp:close(Socket),
    {stop, normal, State};

handle_info(_Info, State=#state{socket=Socket, transport=_Transport}) ->
    io:format("protocol_error~n"),
    ok = gen_tcp:close(Socket),
    {stop, normal, State}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% Private functions.
verify_token(Token) when is_binary(Token) ->
    bank_accounts:check_token(binary_to_list(Token));

verify_token(_) ->
    false.

parse_params(DecodeJson) ->
    Account = proplists:get_value(<<"account">>, DecodeJson),
    Token = verify_token(proplists:get_value(<<"token">>, DecodeJson)),
    CardPin = proplists:get_value(<<"card">>, DecodeJson),

    case {Account, Token, CardPin} of
        {undefined, _, _} ->
            %% io:format("Invalid Account:`~p`", [Account]),
            bank_accounts:notify_error("account key not found");

        {_, false, _} ->
            %% io:format("Invalid Token:`~p`", [Token]),
            bank_accounts:notify_error("invalid token");

        {_, _, undefined} ->
            %% io:format("Invalid CardPin:`~p`", [CardPin]),
            bank_accounts:notify_error("card key not found");

        {Account, _, CardPin} ->
            InitialBalance = proplists:get_value(<<"initial_balance">>, DecodeJson),
            Deposit = proplists:get_value(<<"deposit">>, DecodeJson),
            Withdraw = proplists:get_value(<<"withdraw">>, DecodeJson),
            Balance = proplists:get_value(<<"balance">>, DecodeJson),

            if
                InitialBalance =/= undefined ->
                   [{account, Account, CardPin}, {initial_balance, InitialBalance}];

                Deposit =/= undefined ->
                   [{account, Account, CardPin}, {deposit, Deposit}];

                Withdraw =/= undefined ->
                   [{account, Account, CardPin}, {withdraw, Withdraw}];

                Balance =/= undefined ->
                   [{account, Account, CardPin}, {balance}];

                true -> parser_error
            end
    end.

process_transaction([{account, Account, CardPin}, {initial_balance, Amount}]) ->
    bank_accounts:create_account(binary_to_list(Account),
                                binary_to_list(CardPin), float(Amount));

process_transaction([{account, Account, CardPin}, {deposit, Amount}]) ->
    bank_accounts:deposit(binary_to_list(Account),
                          binary_to_list(CardPin), float(Amount));

process_transaction([{account, Account, CardPin}, {withdraw, Amount}]) ->
    bank_accounts:withdraw(binary_to_list(Account),
                           binary_to_list(CardPin), float(Amount));

process_transaction([{account, Account, CardPin}, {balance}]) ->
    bank_accounts:get_balance(binary_to_list(Account), binary_to_list(CardPin));

process_transaction(Params) ->
    bank_accounts:notify_error(Params).

