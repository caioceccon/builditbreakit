-module(bank_accounts).
% don't lose your ets table.
-behaviour(gen_server).

-compile([debug_info, export_all]).

-export([start_link/0]).
-export([init/1, handle_cast/2, handle_call/3, handle_info/2,
         terminate/2, code_change/3, create_account/3,
         get_balance/2, deposit/3, withdraw/3, notify_error/1]).

%% Client API.
start_link() -> gen_server:start_link({local, accounts}, ?MODULE, [], []).

init([]) ->
    ets:new(tbaccounts, [ordered_set, named_table]),
    Token = create_token(),
    {ok, Token}.

create_token() ->
    {A,B,C} = now(),
    N = node(),
    Token = lists:flatten(io_lib:format("~p-~p.~p.~p",[N,A,B,C])),
    {ok, AuthFile} = application:get_env(auth_file),
    file:write_file(AuthFile, Token),
    io:format("created~n"),
    Token.

create_account(Account, CardPin, Amount) when is_list(Account),
                                              is_list(CardPin),
                                              is_number(Amount) ->
    gen_server:call(accounts, {create_account, Account, CardPin, Amount}).

get_balance(Account, CardPin) when is_list(Account), is_list(CardPin) ->
    gen_server:call(accounts, {balance, Account, CardPin}).

deposit(Account, CardPin, Amount) when is_list(Account),
                                       is_list(CardPin),
                                       is_number(Amount) ->
    gen_server:call(accounts, {deposit, Account, CardPin, Amount}).

withdraw(Account, CardPin, Amount) when is_list(Account),
                                        is_list(CardPin),
                                        is_number(Amount) ->
    gen_server:call(accounts, {withdraw, Account, CardPin, Amount}).

check_token(Token) when is_list(Token) ->
    gen_server:call(accounts, {check_token, Token}).

%% Private functions.
handle_call({create_account, Account, CardPin, Amount},
            _From, State) when is_list(Account),
                               is_list(CardPin),
                               is_number(Amount),
                               Amount >= 10 ->
    {reply, handle_create_account({Account, CardPin, Amount}), State};

handle_call({balance, Account, CardPin}, _From, State) when is_list(Account),
                                                            is_list(CardPin) ->
    {reply, handle_balance({Account, CardPin}), State};

handle_call({deposit, Account, CardPin, Amount}, _From, State) when is_list(Account),
                                                                    is_list(CardPin),
                                                                    is_number(Amount) ->
    {reply, handle_deposit({Account, CardPin, Amount}), State};

handle_call({withdraw, Account, CardPin, Amount}, _From, State) when is_list(Account),
                                                                     is_list(CardPin),
                                                                     is_number(Amount) ->
    {reply, handle_withdraw({Account, CardPin, Amount}), State};

handle_call({check_token, Token}, _From, State) when is_list(Token) ->
    {reply, handle_check_token(Token, State), State};

handle_call(Msg, _From, State) ->
    {reply, notify_error(io_lib:format("Unknown message~p~n", [Msg])), State}.

%% Unknow command
handle_info(_Msg, State) ->
    %%io:format("Unexpected message: ~p~n", [Msg]),
    {noreply, State}.

handle_cast(_Msg, State) ->
    %%io:format("Unexpected message: ~p~n", [Msg]),
    {noreply, State}.

terminate(normal, _State) ->
    ok.

code_change(_OldVsn, {State}, _Extra) ->
    {ok, {State}}.

get_account(Id) ->
    ets:lookup(tbaccounts, Id).

check_account(Id, CardPin) ->
    AccountTuple = get_account(Id),
    case AccountTuple of
        [{_Account, CardPin, _Balance}] -> AccountTuple;
        _ -> []
    end.

handle_check_token(Token, State) when is_list(Token), is_list(State) ->
    Token =:= State.

handle_create_account({Account, CardPin, Amount}) when is_list(Account),
                                                     is_list(CardPin),
                                                     is_number(Amount) ->
    case get_account(Account) of
        [] ->
            ets:insert(tbaccounts, {Account, CardPin, Amount}),
            Msg = <<"{\"initial_balance\": ~.2f, \"account\": \"~s\"}~n">>,
            FormatedMessage = io_lib:format(Msg, [Amount, Account]),
            io:format("~s", [FormatedMessage]),
            FormatedMessage;
        [_] -> notify_error("Duplicated Account")
    end;

handle_create_account(_) ->
    notify_error("handle_create_account unexpected params").

handle_balance({Account, CardPin}) when is_list(Account), is_list(CardPin) ->
    case check_account(Account, CardPin) of
        [{_Account, _Card, Balance}] ->
            Msg = <<"{\"account\": \"~s\", \"balance\": ~.2f}~n">>,
            FormatedMessage = io_lib:format(Msg, [Account, Balance]),
            io:format("~s", [FormatedMessage]),
            FormatedMessage;
        [] -> notify_error("Invalid Account or Card")
    end;

handle_balance(_) ->
    notify_error("handle_balance unexpected params").

handle_deposit({Account, CardPin, Amount}) when is_list(Account),
                                              is_list(CardPin),
                                              is_number(Amount) ->
    case check_account(Account, CardPin) of
        [{Account, Card, Balance}] ->
            NewBalance = Balance + Amount,
            ets:insert(tbaccounts, {Account, Card, NewBalance}),
            Msg = <<"{\"account\": \"~s\", \"deposit\": ~.2f}~n">>,
            FormatedMessage = io_lib:format(Msg, [Account, Amount]),
            io:format("~s", [FormatedMessage]),
            FormatedMessage;
        [] -> notify_error("Invalid Account or Card")
    end;

handle_deposit(_) ->
    notify_error("handle_deposit unexpected params").

handle_withdraw({Account, CardPin, Amount}) when is_list(Account),
                                               is_list(CardPin),
                                               is_number(Amount) ->
    case check_account(Account, CardPin) of
        [{Account, Card, Balance}] ->
            NewBalance = Balance - Amount,
            DecimalString = io_lib:format("~.2f", [NewBalance]),
            DecimalBalance = list_to_float(hd(DecimalString)),
            if
                DecimalBalance >= 0.0 ->
                    ets:insert(tbaccounts, {Account, Card, NewBalance}),
                    Msg = <<"{\"account\": \"~s\", \"withdraw\": ~.2f}~n">>,
                    FormatedMessage = io_lib:format(Msg, [Account, Amount]),
                    io:format("~s", [FormatedMessage]),
                    FormatedMessage;
                true ->
                    ErrorMessage = io_lib:format("Not enough Balance ~f~n", [NewBalance]),
                    notify_error(ErrorMessage)
            end;
        [] -> notify_error("Invalid Account or Card")
    end;

handle_withdraw(_) ->
    notify_error("handle_withdraw unexpected params").

notify_error(Msg) when is_list(Msg) ->
    %%io:format(standard_error, "Error: ~p~n", [Msg]),
    io_lib:format(<<"~p~n">>, [255]).
