-module(bank_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_Type, _Args) ->
    Port = case application:get_env(port) of
        {ok, P} -> list_to_integer(P);
        undefined -> 3000
    end,
    {ok, _} = ranch:start_listener(bank_atm_listener, 10,
        ranch_tcp, [{port, Port}], bank_atm, []),
    bank_sup:start_link().

stop(_State) ->
    ok.