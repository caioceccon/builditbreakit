-module(bank).

%% Application callbacks
-export([main/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

main(Args) ->
    case Args of
        ["-p", Port, "-s", AuthFile] -> main(Port, AuthFile);
        ["-s", AuthFile, "-p", Port] -> main(Port, AuthFile);
        ["-s", AuthFile] -> main("3000", AuthFile);
        ["-p", Port] -> main(Port, "./bank.auth");
        [] -> main("3000", "./bank.auth");
        _-> erlang:halt(255)
    end.

main(Port, AuthFile) ->
    application:set_env(bank, port, Port),
    application:set_env(bank, auth_file, AuthFile),
    ok = application:start(ranch),
    ok = application:start(bank),
    timer:sleep(infinity).

