-module(bank_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(CHILD(I, Type, Args), {I, {I, start_link, Args}, permanent, 5000, Type, [I]}).

start_link() -> supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
    {ok, {{one_for_one, 60, 3600},
       [?CHILD(bank_accounts, worker, [])]
    }}.
