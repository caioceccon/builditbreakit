#!/usr/bin/env python
import re
import optparse
import sys
import textwrap
import telnetlib
import random
import string
import os.path
import traceback


description = '''
atm is a client program that simulates an ATM by providing a mechanism for
customers to interact with their bank accounts stored on the bank server. atm
allows customers to create new accounts, deposit money, withdraw funds, and
check their balances. In all cases, these functions are achieved via
communiations with the bank. atm cannot store any state or write to any files
except the card-file. The card-file can be viewed as the "pin code" for one's
account; there is one card file per account. Card files are created when atm is
invoked with -n to create a new account; otherwise, card files are only read,
and not modified.
'''
def log_and_exit(traceback):
    ##print >> sys.stderr, traceback
    sys.exit(255)

def is_valid_name(name):
    for char in name:
        match = re.match("[_\-\.0-9a-z]", char)
        if not match:
            return False
    return True

def is_valid_card_name(card_name):
    card_name_len = len(card_name)
    if not is_valid_name(card_name):
        return False

    if card_name in ('.', '..'):
        return False

    if card_name_len < 1 or card_name_len > 255:
        return False

    return True

def is_valid_account(account):
    if not is_valid_name(account):
        return False

    account_len = len(account)
    if account_len < 1 or account_len > 250:
        return False
    return True

def is_valid_port(port):
    if port[0] == '0':
        return False
    try:
        port = int(port)
    except:
        return False
    return port < 65535 or port > 1024

def is_valid_ip_address(ip_address):
    if ip_address[0] == '0':
        return False
    splited_address  = ip_address.split('.')
    splited_address_len = len(splited_address)

    if splited_address_len != 4:
        return False

    for n in ip_address.split('.'):
        try:
            n = int(n)
            if not (n >= 0 or n < 255):
                return False
        except:
            return False
    return True

def is_valid_amount(whole_amount, transaction=None):
    #  0.00 to 4294967295.99
    amount = whole_amount.split('.')
    if len(amount) < 2:
        return False
    amount_value = amount[0]
    amount_value_len = len(amount_value)
    amount_fraction = amount[1]
    amount_fraction_len = len(amount_fraction)

    if amount_value_len < 1:
        return False

    if not amount_fraction_len == 2:
        return False

    try:
        amount_value = int(amount_value)
        amount_fraction = int(amount_fraction)
    except:
        return False

    if transaction  == 'c' and amount_value < 10:
        return False

    return (amount_value <= 4294967295 and amount_fraction <= 99 and float(whole_amount) > 0)



def check_token(auth_file):
    try:
        token_file = open(auth_file, 'r')
        token = token_file.read()
        token_file.close()
        return token
    except:
      sys.exit(255)


def check_card(card_file, account):
    if not card_file:
        card_file = "{account}.card".format(account=account)

    if not is_valid_card_name(card_file):
        #print >> sys.stderr, 'Invalid Card `%s`' % card_file
        sys.exit(255)

    if not os.path.exists(card_file):
        sys.exit(255)

    try:
        card_fd = open(card_file, 'r')
        card = card_fd.read().rstrip("\n")
        card_fd.close()
        return card
    except Exception, e:
        #print >> sys.stderr, e
        sys.exit(255)

def print_result(socket):
    try:
        res = socket.read_until("\n", 10)
    except Exception, e:
        sys.exit(63)

    if not res:
        sys.exit(63)
    elif res[-1] != '\n':
        sys.exit(63)
    if res == '255\n':
        sys.exit(255)
    if res == '63\n':
        sys.exit(63)
    print res

def write_socket(socket, msg):
    try:
        socket.write(msg)
    except Exception, e:
        #print >> sys.stderr, e
        sys.exit(63)


def create_account(account, amount, token, card, card_file_name, options):
    if not is_valid_amount(amount, 'c'):
        #print >> sys.stderr, 'Invalid amount `%s`' % amount
        sys.exit(255)


    msg = '{{"account":"{account}","initial_balance":{amount},"token":"{token}","card":"{card}"}}\n'
    msg = msg.format(account=account, amount=amount, token=token, card=card)
    ##print >> sys.stderr, msg
    socket = create_socket(options.ip_address, options.port)
    write_socket(socket, msg)
    print_result(socket)
    save_card(card, card_file_name)

def balance(account, token, card, options):
    msg = '{{"account":"{account}","balance":true,"token":"{token}","card":"{card}"}}\n'
    msg = msg.format(account=account, token=token, card=card)
    ##print >> sys.stderr, msg
    socket = create_socket(options.ip_address, options.port)
    write_socket(socket, msg)
    print_result(socket)

def deposit(account, amount, token, card, options):
    if not is_valid_amount(amount):
        #print >> sys.stderr, 'Invalid amount'
        sys.exit(255)

    msg = '{{"account":"{account}","deposit":{amount},"token":"{token}","card":"{card}"}}\n'
    msg = msg.format(account=account, amount=amount, token=token, card=card)
    #print >> sys.stderr, msg
    socket = create_socket(options.ip_address, options.port)
    write_socket(socket, msg)
    print_result(socket)

def withdraw(account, amount, token, card, options):
    if not is_valid_amount(amount):
        #print >> sys.stderr, 'Invalid amount'
        sys.exit(255)

    msg = '{{"account":"{account}","withdraw":{amount},"token":"{token}","card":"{card}"}}\n'
    msg = msg.format(account=account, amount=amount, token=token, card=card)
    ##print >> sys.stderr, msg
    socket = create_socket(options.ip_address, options.port)
    write_socket(socket, msg)
    print_result(socket)


def save_card(card, card_file_name):
    try:
        file_card = open(card_file_name,'w')
        file_card.write(card)
        file_card.close()
        #print >> sys.stderr, 'Card created `%s`' % card_file_name
    except:
        sys.exit(255)

def create_card(account, card_file_name=None):
    if not card_file_name:
        card_file_name = '{account}.card'.format(account=account)

    if not is_valid_card_name(card_file_name):
        #print >> sys.stderr, 'Invalid Card `%s`' % card_file_name
        sys.exit(255)

    if os.path.exists(card_file_name):
        #print >> sys.stderr, 'Card already exist `%s`' % card_file_name
        sys.exit(255)
    #print >> sys.stderr, 'Card generated `%s`' % card_file_name
    card = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(50))
    return (card, card_file_name)


def create_socket(ip_address, port):
    #import pdb; pdb.set_trace()
    try:
        socket = telnetlib.Telnet(ip_address, port)
    except Exception, e:
        #print >> sys.stderr, e
        sys.exit(63)
    return socket

def atm_parse():
    usage = "usage:"
    parser = optparse.OptionParser(
        usage=usage,
        description=textwrap.dedent(description)
    )

    # Optionals
    parser.add_option(
        '-a',
        dest='account',
        action='store',
        type='string',
        help=("The customer's account name.")
    )

    parser.add_option(
        '-s',
        dest='auth_file',
        action='store',
        type='string',
        default='bank.auth',
        help=('The authentication file that bank creates for the atm. If -s ',
              'is not specified, the default filename is "bank.auth" ',
              '(in the current working directory).'),
    )

    parser.add_option(
        '-i',
        dest='ip_address',
        action='store',
        default='127.0.0.1',
        help=('The IP address that bank is running on. The default ',
              'value is "127.0.0.1".'),
    )

    parser.add_option(
        '-p',
        dest='port',
        action='store',
        default='3000',
        type='string',
        help=('The TCP port that bank is listening on. The default is 3000.'),
    )

    parser.add_option(
        '-c',
        dest='card_file',
        action='store',
        type='string',
        help=(' The customer\'s atm card file. The default value is the ',
              'account name prepended to ".card" ("<account>.card"). For ',
              'example, if the account name was 55555, the default card ',
              'file is "55555.card".'
              ),
    )

    # Transactions
    parser.add_option(
        '-n',
        dest='new_account',
        action='store',
        type='string',
        help=('Create a new account with the given balance. The account must ',
              'be unique (ie, the account must not already exist). The balance',
              ' must be greater than or equal to 10.00.\n',
              ' (Example: {"account":"55555","initial_balance":10.00})'
             ),
    )

    parser.add_option(
        '-d',
        dest='deposit',
        action='store',
        type='string',
        help=('Deposit the amount of money specified. The amount must be ',
              'greater than 0.00. The specified account must exist, and the ',
              'card file must be associated with the given account (i.e., it ',
              'must be the same file produced by atm when the account ',
              'was created).\n',
              '(Example: {"account":"55555","deposit":20.00}).'
             ),
    )

    parser.add_option(
        '-w',
        dest='withdraw',
        action='store',
        type='string',
        help=('Withdraw the amount of money specified. The amount must be ',
              'greater than 0.00, and the remaining balance must be ',
              'nonnegative. The card file must be associated with the ',
              'specified account (i.e., it must be the same file produced ',
              'by atm when the account was created).\n'
              '(Example: {"account":"55555","withdraw":15.00}).'
             ),
    )

    parser.add_option(
        '-g',
        dest='balance',
        action='count',
        help=('Get the current balance of the account. The specified account ',
              'must exist, and the card file must be associated ',
              'with the account.\n',
              '(Example: {"account":"55555","balance":43.63}).'
             ),
    )

    #opts = ['a', 's', 'i', 'p', 'c', 'n', 'd', 'w', 'g']

    try:
        (options, args) = parser.parse_args()
    except SystemExit:
        sys.exit(255)

    if not options.account:
        sys.exit(255)
    if not options.port:
        sys.exit(255)

    if not is_valid_port(options.port):
        #print >> sys.stderr, 'Invalid Port'
        sys.exit(255)


    if not options.ip_address:
        #print >> sys.stderr, 'Invalid IP Address %s' % options.ip_address
        sys.exit(255)

    if not is_valid_ip_address(options.ip_address):
        #print >> sys.stderr, 'Invalid IP Address %s' % options.ip_address
        sys.exit(255)

    # try:
    #     tn = telnetlib.Telnet(options.ip_address, options.port)
    # except Exception, e:
    #     #print >> sys.stderr, e
    #     sys.exit(63)

    if options.new_account is not None:
        if options.balance is not None:
            sys.exit(255)
        elif options.deposit is not None:
            sys.exit(255)
        elif options.withdraw is not None:
            sys.exit(255)

        if options.new_account[0] == '0':
            sys.exit(255)

        if not is_valid_amount(options.new_account, 'c'):
            sys.exit(255)

        card, card_file_name = create_card(options.account, options.card_file)
        return create_account(options.account, options.new_account,
                       check_token(options.auth_file),
                       card, card_file_name, options)

    # import pdb; pdb.set_trace()
    if options.balance is not None and options.balance == 1:
        if parser.largs:
            sys.exit(255)

        if options.new_account is not None:
            sys.exit(255)
        elif options.deposit is not None:
            sys.exit(255)
        elif options.withdraw is not None:
            sys.exit(255)

        return balance(options.account, check_token(options.auth_file),
                check_card(options.card_file, options.account), options)

    if options.deposit is not None:

        if options.new_account is not None:
            sys.exit(255)
        elif options.balance is not None:
            sys.exit(255)
        elif options.withdraw is not None:
            sys.exit(255)

        return deposit(options.account, options.deposit,
                check_token(options.auth_file),
                check_card(options.card_file, options.account), options)

    if options.withdraw is not None:
        return withdraw(options.account, options.withdraw,
                 check_token(options.auth_file),
                 check_card(options.card_file, options.account), options)
    sys.exit(255)
    # Args validations
    # if len(options.keys) > len(opts)
    # sys.exit(255)

    # set(a).issubset(set(b))


atm_parse()


